"""Test MessageQueue."""
import json
import ssl
import unittest
from unittest import mock

import pika

from cki_lib import messagequeue


class TestMessageQueue(unittest.TestCase):
    """Test cki_lib/messagequeue.py."""

    def test_init(self):
        """Test MessageQueue init."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        credentials = pika.PlainCredentials('user', 'password')
        connection_params = pika.ConnectionParameters(
            host='host', port=123, credentials=credentials
        )

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_default(self):
        """Test MessageQueue init default values."""
        queue = messagequeue.MessageQueue('host')
        credentials = pika.PlainCredentials('guest', 'guest')
        connection_params = pika.ConnectionParameters(
            host='host', port=5672, credentials=credentials
        )

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_ssl_443(self):
        """Test MessageQueue init default values with ssl."""
        queue = messagequeue.MessageQueue('host', port=443)
        credentials = pika.PlainCredentials('guest', 'guest')
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = pika.ConnectionParameters(
            host='host', port=443, credentials=credentials,
            ssl_options=ssl_options
        )

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_ssl_5671(self):
        """Test MessageQueue init default values with ssl."""
        queue = messagequeue.MessageQueue('host', port=5671)
        credentials = pika.PlainCredentials('guest', 'guest')
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = pika.ConnectionParameters(
            host='host', port=5671, credentials=credentials,
            ssl_options=ssl_options
        )

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @mock.patch('ssl.SSLContext.load_cert_chain')
    def test_init_ssl_cert(self, load_cert_chain):
        """Test MessageQueue init with ssl cert."""
        certfile = 'certfile'
        queue = messagequeue.MessageQueue(
            'host', port=443, certfile=certfile)
        credentials = pika.credentials.ExternalCredentials()
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = pika.ConnectionParameters(
            host='host', port=443, credentials=credentials,
            ssl_options=ssl_options
        )

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)
        load_cert_chain.assert_called_with(certfile)

    def test_init_ssl_custom(self):
        """Test MessageQueue init default values with custom ssl."""
        ssl_options = pika.SSLOptions(ssl.create_default_context(), 'host')
        queue = messagequeue.MessageQueue(
            'host', port=443, connection_params={'ssl_options': ssl_options})
        credentials = pika.PlainCredentials('guest', 'guest')
        connection_params = pika.ConnectionParameters(
            host='host', port=443, credentials=credentials,
            ssl_options=ssl_options
        )

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @mock.patch('pika.BlockingConnection')
    def test_connect(self, blockingconnection):
        # pylint: disable=no-self-use
        """Test connection parameters."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')

        with queue.connect():
            blockingconnection.assert_called_with(queue.connection_params)

    @mock.patch('pika.ConnectionParameters')
    def test_connection_params(self, connection_params):
        # pylint: disable=no-self-use
        """Test connection params kwarg on init."""
        queue = messagequeue.MessageQueue(
            'host', 123, 'user', 'password',
            connection_params={'extraparam': 'value',
                               'someotherparam': 'othervalue'}
        )
        connection_params.assert_called_with(
            credentials=queue.credentials, host='host', port=123,
            extraparam='value', someotherparam='othervalue'
        )

    @mock.patch('pika.BlockingConnection')
    def test_connect_context_manager(self, connection):
        """Test context manager."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')

        with queue.connect():
            connection.assert_called_with(queue.connection_params)
        self.assertTrue(connection().close.called)

    @mock.patch('pika.BlockingConnection')
    def test_queue_init(self, connection):  # pylint: disable=no-self-use
        """Test queue_init."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')

        queue.queue_init('queue_name')
        connection().channel().queue_declare.assert_called_with('queue_name')

        queue.queue_init('queue_name', params={'durable': True})
        connection().channel().queue_declare.assert_called_with('queue_name',
                                                                durable=True)

    @mock.patch('pika.BlockingConnection')
    def test_send_message(self, connection):  # pylint: disable=no-self-use
        """Test send_message."""
        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')

        queue.send_message(
            {'key': 'value'}, 'queue_name', exchange='exchange_name'
        )
        connection().channel().basic_publish.assert_called_with(
            body='{"key": "value"}',
            exchange='exchange_name',
            routing_key='queue_name'
        )

        # Test default exchange.
        queue.send_message({'key': 'value'}, 'queue_name')
        connection().channel().basic_publish.assert_called_with(
            body='{"key": "value"}', exchange='', routing_key='queue_name'
        )

    @mock.patch('pika.BlockingConnection')
    def test_consume(self, connection):
        """Test consume_messages."""
        self._test_consume(connection)

    @mock.patch('pika.BlockingConnection')
    def test_consume_prefetch(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, prefetch_count=10)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('pika.BlockingConnection')
    def test_consume_production_no_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('pika.BlockingConnection')
    def test_consume_production_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, queue_name='queue',
                           is_production=True)

    @mock.patch('pika.BlockingConnection')
    def test_consume_no_production_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, queue_name='queue',
                           is_production=False)

    @mock.patch('pika.BlockingConnection')
    def test_consume_inactivity_timeout(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, inactivity_timeout=60)

    # pylint: disable=no-self-use,too-many-arguments
    def _test_consume(self, connection, prefetch_count=None,
                      queue_name=None, is_production=False,
                      inactivity_timeout=None):
        exchange = 'exchange'
        routing_keys = ['routing1', 'routing2']
        values = ['value1', 'value2']
        connection().channel().consume.return_value = [
            (mock.Mock(routing_key=f'r{v}', delivery_tag=f't{v}'),
             'properties', json.dumps({'b': v})) for v in values]
        if inactivity_timeout:
            connection().channel().consume.return_value += [
                (None, None, None), ('invalid',)]
        callback = mock.Mock(side_effect=[Exception('boom'), mock.DEFAULT])

        queue = messagequeue.MessageQueue('host', 123, 'user', 'password')
        queue.consume_messages(exchange, routing_keys, callback,
                               prefetch_count=prefetch_count,
                               queue_name=queue_name,
                               inactivity_timeout=inactivity_timeout)

        connection.assert_called_with(queue.connection_params)
        if prefetch_count:
            connection().channel().basic_qos.assert_called_with(
                prefetch_count=prefetch_count)
        if is_production and queue_name:
            connection().channel().queue_declare.assert_called_with(
                queue_name, durable=True)
        else:
            connection().channel().queue_declare.assert_called_with(
                mock.ANY, exclusive=True)
        connection().channel().queue_bind.assert_has_calls([
            mock.call(mock.ANY, exchange, routing_key=r)
            for r in routing_keys])
        if inactivity_timeout:
            connection().channel().consume.assert_called_with(
                mock.ANY, inactivity_timeout=inactivity_timeout)
        self.assertEqual(connection().channel().basic_ack.mock_calls, [
            mock.call(f't{values[1]}')])
        self.assertEqual(callback.mock_calls,
                         [mock.call(f'r{v}', {'b': v}) for v in values])
