#!/bin/bash

set -euo pipefail

echo "PID shell: $$"

if [ -v START_REDIS ]; then
    /usr/bin/redis-server /etc/redis.conf &
    echo "PID redis: $!"
fi

for CELERY_VAR in "${!START_CELERY_@}"; do
    IFS=' ' read -r -a ARGS <<< "${!CELERY_VAR}"
    celery worker "${ARGS[@]}" &
    echo "PID $CELERY_VAR: $!"
done

if [ -v START_FLASK ]; then
    if [[ "${IS_PRODUCTION:-}" = [Tt]rue ]] ; then
        gunicorn --bind 0.0.0.0:5000 --workers "${WEB_WORKERS:-2}" "${START_FLASK}:app" &
    else
        FLASK_APP=${START_FLASK} FLASK_ENV=development flask run --host 0.0.0.0 --port 5000 &
    fi
    echo "PID flask: $!"
fi

if [ -v START_PYTHON ]; then
    IFS=' ' read -r -a ARGS <<< "${START_PYTHON}"
    python3 -m "${ARGS[@]}" &
    echo "PID python module: $!"
fi

# Tell the whole process group that it's time to quit.
trap 'trap - SIGINT SIGTERM EXIT && kill -- -$$' SIGINT SIGTERM EXIT

# Wait for one process to exit; the trap should take care of all other ones
wait -n
