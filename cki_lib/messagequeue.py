"""RabbitMQ Message Queue helper."""
import contextlib
import json
import ssl
import uuid

import pika

from . import logger
from . import misc

LOGGER = logger.get_logger(__name__)


class MessageQueue:
    """
    RabbitMQ message queue helper.

    Helper to handle queue initialization and message sending.
    MessageQueue.connect() should be used to get a context manager for a
    `pika.channel.Channel`.

    host: RabbitMQ server address.
    port: RabbitMQ server port. Defaults to 5672.
          With a port of 443 or 5671, uses SSL.
    user: RabbitMQ server user. Defaults to guest.
    password: RabbitMQ server password. Defaults to guest.
    cafile: ca certificates
    certfile: SSL client private key and corresponding certificate.
    """

    def __init__(self, host, port=5672, user='guest', password='guest',
                 connection_params=None, cafile=None, certfile=None):
        # pylint: disable=too-many-arguments
        """Init."""
        connection_params = dict(connection_params or {})
        if (port in (443, 5671) or certfile or cafile) and \
                'ssl_options' not in connection_params:
            connection_params['ssl_options'] = pika.SSLOptions(
                ssl.create_default_context(cafile=cafile))
        if certfile:
            self.credentials = pika.credentials.ExternalCredentials()
            connection_params['ssl_options'].context.load_cert_chain(certfile)
        else:
            self.credentials = pika.PlainCredentials(user, password)
        self.connection_params = pika.ConnectionParameters(
            host=host, port=port, credentials=self.credentials,
            **connection_params)

    def queue_init(self, name, params=None):
        """Create queue on remote server."""
        with self.connect() as channel:
            channel.queue_declare(name, **(params or {}))

    def send_message(self, data, queue_name, exchange=''):
        """
        Send message to queue.

        Encode `data` as json and send it to routing_key=queue_name,
        exchange=exchange.
        """
        body = json.dumps(data)
        with self.connect() as channel:
            channel.basic_publish(
                exchange=exchange, routing_key=queue_name, body=body
            )

    # pylint: disable=too-many-arguments
    def consume_messages(self, exchange, routing_keys, callback,
                         queue_name=None, prefetch_count=None,
                         inactivity_timeout=None):
        """Endlessly consume messages."""
        with self.connect() as channel:
            if prefetch_count:
                channel.basic_qos(prefetch_count=prefetch_count)
            if misc.is_production() and queue_name:
                # production queue, as durable as possible to not lose messages
                channel.queue_declare(queue_name, durable=True)
            else:
                # temporary queue, uuid format for fedora-messaging
                queue_name = str(uuid.uuid4())
                channel.queue_declare(queue_name, exclusive=True)
            for routing_key in routing_keys:
                channel.queue_bind(queue_name, exchange,
                                   routing_key=routing_key)
            for method, _, body in channel.consume(
                    queue_name, inactivity_timeout=inactivity_timeout):
                if not method:  # inactivity timeout
                    return
                try:
                    callback(method.routing_key, json.loads(body))
                    channel.basic_ack(method.delivery_tag)
                # pylint: disable=broad-except
                except Exception:
                    LOGGER.exception('Message handling failure, '
                                     'will be requeued after restart')

    @contextlib.contextmanager
    def connect(self):
        """Connect to the server and return a channel."""
        connection = pika.BlockingConnection(self.connection_params)
        try:
            yield connection.channel()
        finally:
            connection.close()
