#!/usr/bin/env python3
"""psql utility wrapper"""
import copy
import logging
import os
import shlex
import sys
import time

from multiprocessing import Event
from multiprocessing import Process
from multiprocessing import Queue
from queue import Empty

from bs4 import BeautifulSoup as BS
from twisted.internet import error, protocol, reactor

from cki_lib.logger import file_logger

LOGGER = file_logger(__name__, dst_file='ps_teiid_info.log',
                     stream_level=logging.INFO, stream=sys.stdout)

STDERR_LOGGER = file_logger(__name__ + '_stderr',
                            dst_file='ps_teiid_info_stderr.log',
                            stream_level=logging.INFO, stream=sys.stderr)


# pylint: disable=E1101

def is_output_done(data):
    """Return True if string contains output termination marker."""
    return ('row)' in data or 'rows)'
            in data or 'INSERT' in data or
            'ERROR: ' in data)


def _handle_stderr(stderr_data):
    """Check stderr for known errors, return False on unknown."""
    continue_execution = True
    invalid_query = False

    if 'Was expecting:' in stderr_data:
        # User used invalid sql query
        invalid_query = True
        # Error handled, don't break execution completely
        continue_execution = True
        # Log data
        LOGGER.warning(stderr_data)

    elif 'duplicate key value violates unique constraint' in stderr_data or \
            'ERROR:' in stderr_data:
        # User used invalid sql query
        invalid_query = True
        # Query would break db integrity, let's not continue; let it
        # explode so user fixes whatever is wrong.
        continue_execution = False
        # Log data
        LOGGER.info(stderr_data)
    elif 'could not find a "psql" to execute' in stderr_data:
        # Not a problem with sql query
        invalid_query = False
        # Error handled, don't break execution completely
        continue_execution = True

        try:
            _ = _handle_stderr.psql_warn_printed
        except AttributeError:
            _handle_stderr.psql_warn_printed = True
            LOGGER.info(f'WARN (known error): {stderr_data.strip()}')
    elif 'server closed the connection unexpectedly' in stderr_data:
        continue_execution = False
        LOGGER.info(stderr_data.strip())
    elif stderr_data:
        # Break execution on unknown errors
        continue_execution = False
        LOGGER.info(stderr_data.strip())

    return continue_execution, invalid_query


class TeiidConnector(protocol.ProcessProtocol):
    # pylint: disable=R0902
    """Uses psql to provide a connector to TEIID."""

    def __init__(self, command, timeout=5, env=copy.deepcopy(os.environ)):
        """Create the object and psql process."""
        self.command = shlex.split(command)
        self.env = env
        # weird warning
        self.psql_warn_printed = False

        # All response that we've received to current query
        self.current_query_data = ''
        # Finalized query result
        self.final_data = ''

        self.invalid_query = False

        self.shutdown_flag = Event()
        self.last_epoch = time.time()

        self.queue = Queue()
        self.results = Queue()

        self.timeout = timeout

        self.proc = None

    @classmethod
    def html2rows_cols(cls, data):
        """Read html data from psql, convert to rows/cols lists."""
        rows = []
        soup = BS(data, "lxml")

        cols = [x.text for x in soup.findAll('th')]

        for table_rows in soup.findAll('tr')[1:]:
            rows.append([x.text for x in table_rows.findAll('td')])

        return rows, cols

    def _write_query(self, query):
        # pylint: disable=W1202
        """Write query to stdin of psql."""
        query = query.replace('\n', ' ').replace('\t', ' ')
        if not query.strip().endswith(';'):
            query = query + ';'
        # always add a newline, because it was removed above
        query = query + '\n'

        LOGGER.debug('Running %s', query)

        # Write query to stdin
        self.transport.write(query.encode('utf-8'))

        return query

    def errReceived(self, data):
        # pylint: disable=C0103
        """Process stderr stream."""
        data = data.decode('utf-8')

        continue_execution, self.invalid_query = _handle_stderr(data)
        if not continue_execution:
            self.shutdown()

    def shutdown(self):
        """End process."""
        if not self.shutdown_flag.is_set():
            self.shutdown_flag.set()
            LOGGER.debug('doing shutdown')
            self.proc.signalProcess('TERM')

    def check_timeout(self):
        """Check if more than timeout_length passed since last stdout data."""
        if self.timeout:
            return int(time.time() - self.last_epoch) >= self.timeout

        return False

    def outReceived(self, data):
        # pylint: disable=C0103
        """Process stdout stream."""
        data = data.decode('utf-8')

        max_frame = self.current_query_data + data
        if is_output_done(max_frame):
            self.current_query_data = ''
            self.final_data = max_frame
        else:
            self.current_query_data = max_frame

        # Reset time counter
        self.last_epoch = time.time()

    def _send_queries(self):
        # pylint: disable=R1723
        """Send query to TEIID."""

        while True:

            timeout = self.check_timeout()
            shutdown = self.shutdown_flag.is_set()
            if timeout or shutdown:
                break
            elif not self.queue.empty():
                query = self.queue.get()
                if query.strip().lower() == 'exit':
                    self.shutdown()
                    break
                else:
                    reactor.callFromThread(self._write_query, query)
            else:
                time.sleep(1)

    def _fetch_results(self):
        while True:
            timeout = self.check_timeout()
            shutdown = self.shutdown_flag.is_set()
            if self.final_data or self.invalid_query or timeout or shutdown:
                if timeout or shutdown:
                    self.final_data = self.current_query_data
                    self.current_query_data = ''

                self.results.put(self.html2rows_cols(self.final_data))
                self.final_data = ''
                # reset flag
                self.invalid_query = False
                if timeout or shutdown:
                    break
            else:
                time.sleep(1)

    def prepare(self):
        """Prepare teiid protocol to start."""
        self.proc = reactor.spawnProcess(self,
                                         os.path.basename(self.command[0]),
                                         self.command,
                                         env=self.env if self.env else
                                         os.environ)
        reactor.callInThread(self._fetch_results)
        reactor.callInThread(self._send_queries)

    def _run(self):
        """Run teiid protocol."""
        self.prepare()
        reactor.run()

    def run(self):
        """Run teiid protocol in a separate process."""
        proc = Process(target=self._run)
        proc.start()

        return proc

    def processEnded(self, reason):
        # pylint: disable=E1101,C0326,C0103
        """Called when psql process ended; stop twisted reactor."""
        # Log that console process finished running
        LOGGER.debug(f'* teiid process exited: {str(reason)}')

        self.shutdown_flag.set()
        try:
            reactor.stop()
        except error.ReactorNotRunning:
            pass

    def query(self, query, block=True):
        """Send query to teiid and get result."""
        self.queue.put(query, block=block)

        try:
            return self.results.get(block=block)
        except Empty:
            return None, None
