#!/usr/bin/env python3
"""Gather pipeline data for futher processing."""

from multiprocessing import Pool

from cached_property import cached_property
from rcdefinition import const
from rcdefinition.rc_data import SKTData

from cki_lib.gitlab import GitlabHelper
from cki_lib.logger import file_logger
from cki_lib.misc import partition

LOGGER = file_logger(__name__, dst_file='info.log')


class Pipeline:
    """Wrapper that describes how to access pipeline data."""

    def __init__(self, gitlab_url, gitlab_token, project, pipeline_id):
        """Initialize the object."""
        self.gitlab_url = gitlab_url
        self.project = project
        self.pipeline_id = pipeline_id

        self.gitlab_token = gitlab_token
        self.gitlab_helper = GitlabHelper(self.gitlab_url, self.gitlab_token)
        self.gitlab_helper.set_project(project)
        self._pipe = self.gitlab_helper.project.pipelines.get(pipeline_id)

    @cached_property
    def last_stage_run(self):
        """Find the last stage that ran in the pipeline.

        Returns: str, name of the stage like 'test' or None
        """
        for stage in const.JOB_STAGES:
            # If at least one job ran in this stage, then return it as the
            # last stage that ran in the pipeline.
            if [x for x in self.raw_jobs if x['stage'] == stage]:
                LOGGER.info(
                    f"Last stage run for pipeline {self.project} "
                    f"#{self.pipeline_id}: {stage}"
                )
                return stage

        # Give up if we could not find a stage with jobs.
        return None

    @cached_property
    def reportable_jobs(self):
        """Get a list of jobs from the pipeline, excluding old retried jobs.

        Raises: PipelineException if there are no jobs in the lint + last stage
        Returns: a list of jobs from last stage and the lint stage
        """
        # Get a list of jobs that match the last stage ran + lint stage (if
        # available) and sort them by their id (latest first).
        jobs = sorted(
            [x for x in self.raw_jobs if x['stage'] in [self.last_stage_run,
                                                        const.LINT_STAGE]],
            reverse=True, key=lambda k: k['id']
        )
        # Remove older jobs that were retried.
        latest_jobs = self._filter_raw_jobs_retried(jobs)

        # If we have no jobs at this point, we have no work left to do.
        if not latest_jobs:
            raise PipelineException("No jobs found in last stage")

        return latest_jobs

    @cached_property
    def raw_rc_data(self):
        """Gather all the available rc files from the pipeline.

        Files are downloaded in parallel and parsed.
        """
        LOGGER.info(
            f"Downloading rc data for {self.project} #{self.pipeline_id}"
        )

        with Pool(len(self.reportable_jobs)) as download_pool:
            job_rc_data_tuples = download_pool.map(self.get_rc_data,
                                                   self.reportable_jobs)

        return job_rc_data_tuples

    def get_rc_data(self, job):
        """Download testing rc data from a pipeline job and parse it.
        Raises:
            PipelineException when the job took too long to execute
            PipelineException when there was no rc-file
        """
        # pylint: disable=too-many-branches
        LOGGER.debug("Getting job data for job %s", job['id'])

        # Download the rc file.
        rc_text = self.gitlab_helper.get_artifact(job['id'], 'rc', raw=False)
        if rc_text:
            skt_data = SKTData.deserialize(rc_text)

        # Raise an exception if we didn't get our rc data for this job.
        if not rc_text or not skt_data:
            # task: handle issues better and with more detail, possibly using
            # regexes we have.
            if job['duration'] >= 24 * 3600:
                raise PipelineException(f"Timeout in job {job['web_url']}")
            raise PipelineException(f"Missing rc file in job {job['web_url']}")

        return (job, skt_data)

    @cached_property
    def attributes(self):
        """Get basic data about the pipeline."""
        LOGGER.info(
            f"Getting pipeline attributes for {self.project} "
            f"#{self.pipeline_id}"
        )
        attributes = self._pipe.attributes
        if not isinstance(attributes, dict):
            raise PipelineException(
                f"Pipeline {self.project} #{self.pipeline_id} could not be"
                f"found."
            )

        if attributes['status'] not in ['success', 'failed']:
            raise PipelineException(
                f"Pipeline {self.project} #{self.pipeline_id} has an "
                f"invalid status: {attributes['status']}"
            )

        return attributes

    @cached_property
    def raw_jobs(self):
        """Get the latest list of jobs from GitLab for this pipeline."""
        LOGGER.info(
            f"Getting pipeline jobs for {self.project} #{self.pipeline_id}"
        )
        raw_jobs = self.gitlab_helper.get_raw_jobs(self.pipeline_id)
        # If there are no jobs in this pipeline, we have nothing to do. This
        # was likely due to a YAML error of some sort.
        if not raw_jobs:
            raise PipelineException(
                f"No jobs found for pipeline {self.project} "
                f"#{self.pipeline_id}"
            )

        return raw_jobs

    @classmethod
    def _filter_raw_jobs_retried(cls, raw_jobs):
        """Filter-out jobs that were retried."""
        partition_by_name = partition(raw_jobs, lambda m: m['name'])
        for key in partition_by_name.keys():
            partition_by_name[key] = max(partition_by_name[key],
                                         key=lambda job: job['id'])

        return list(partition_by_name.values())

    @cached_property
    def pipeline_variables(self):
        """Get the pipeline's variables."""
        variables = self._pipe.variables.list(all=True)

        return {var.key: var.value for var in variables}

    @cached_property
    def commit_message(self):
        """Get commit message of commit that triggered pipeline."""
        return self.raw_jobs[0]['commit']['message']


class PipelineException(Exception):
    """Raise when a pipeline exception occurs."""
